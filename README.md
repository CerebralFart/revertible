# Revertible
A simple java package allowing you to revert a class to a previous state.

## Usage
```
class DAO extends Revertible {
	private String field = "initial value";
	...
}
```
```
public static void main(String[] args){
	DAO dao = new DAO();
	dao.setRollbackPoint();
	dao.setField("new value");
	dao.rollback();
	System.out.println(dao.getField()); //initial value
}
```