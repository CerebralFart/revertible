package cf;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RevertibleTest {
    @BeforeEach
    void setup() {
        Revertible.setErrorOnRevertToNull(false);
    }

    @Test
    void testGetSetErrorOnRevertToNull() {
        Assertions.assertFalse(Revertible.isErrorOnRevertToNull());
        Revertible.setErrorOnRevertToNull(true);
        Assertions.assertTrue(Revertible.isErrorOnRevertToNull());
    }

    @Test
    void testErrorOnRevertToNull() {
        Revertible.setErrorOnRevertToNull(true);
        RevertibleMock obj = new RevertibleMock();
        Assertions.assertThrows(RollbackError.class, obj::rollback);
    }

    @Test
    void testRevertToNull() {
        RevertibleMock obj = new RevertibleMock();
        Assertions.assertDoesNotThrow(obj::rollback);
        Assertions.assertNull(obj.getProp1());
        Assertions.assertFalse(obj.isProp2());
        Assertions.assertEquals(0, obj.getProp3());
    }

    @Test
    void testPropRevert() {
        RevertibleMock obj = new RevertibleMock();
        obj.setProp1("pre");
        obj.setProp2(false);
        obj.setProp3(3);
        obj.setRollbackPoint();
        obj.setProp1("post");
        obj.setProp2(true);
        obj.setProp3(5);
        obj.rollback();
        Assertions.assertEquals("pre", obj.getProp1());
        Assertions.assertFalse(obj.isProp2());
        Assertions.assertEquals(3, obj.getProp3());
    }

    @Test
    void testPropRevertToNull() {
        RevertibleMock obj = new RevertibleMock();
        obj.setProp1("val");
        obj.setProp2(true);
        obj.setProp3(3);
        obj.rollback();
        Assertions.assertEquals("val", obj.getProp1());
        Assertions.assertTrue(obj.isProp2());
        Assertions.assertEquals(3, obj.getProp3());
    }

    @Test
    void testStaticProp() {
        RevertibleMock obj = new RevertibleMock();
        RevertibleMock.setStaticProp("value1");
        obj.setRollbackPoint();
        RevertibleMock.setStaticProp("value2");
        obj.rollback();
        Assertions.assertEquals("value2", RevertibleMock.getStaticProp());
    }

    @Test
    void testTransientProp() {
        RevertibleMock obj = new RevertibleMock();
        obj.setTransientProp("value1");
        obj.setRollbackPoint();
        obj.setTransientProp("value2");
        obj.rollback();
        Assertions.assertEquals("value2", obj.getTransientProp());
    }
}
