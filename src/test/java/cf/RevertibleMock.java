package cf;

import lombok.Getter;
import lombok.Setter;

public class RevertibleMock extends Revertible {
    @Setter
    @Getter
    private static String staticProp;

    @Setter
    @Getter
    private transient String transientProp;

    @Setter
    @Getter
    private String prop1;
    @Setter
    @Getter
    private boolean prop2;
    @Setter
    @Getter
    private int prop3;
}
