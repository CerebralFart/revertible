package cf;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

@Slf4j
//TODO move to external util package
public abstract class Revertible {
    @Setter
    @Getter
    private static boolean errorOnRevertToNull = false;

    private transient Map<Field, Object> values;

    public void setRollbackPoint() {
        log.debug("Setting rollback point for {}", this.getID());
        Field[] fields = this.getClass().getDeclaredFields();
        this.values = new HashMap<>(fields.length);
        for (Field field : fields) {
            int modifiers = field.getModifiers();
            if (!Modifier.isStatic(modifiers) && !Modifier.isTransient(modifiers)) {
                withExposedField(field, exposed -> this.values.put(field, field.get(this)));
            }
        }
    }

    public void rollback() {
        log.debug("Reverting {} to last rollback point", this.getID());
        if (this.values == null) {
            log.warn("No rollback point found for {}", this);
            if (errorOnRevertToNull) throw new RollbackError("No rollback point found");
        } else {
            this.values.forEach((field, value) -> withExposedField(field, exposed -> exposed.set(this, value)));
        }
    }

    private static void withExposedField(Field field, Consumer consumer) {
        boolean access = field.isAccessible();
        field.setAccessible(true);
        try {
            consumer.accept(field);
        } catch (IllegalAccessException e) {
            log.error("IllegalAccessException thrown on exposed field '{}'", field.getName(), e);
        }
        field.setAccessible(access);
    }

    private String getID() {
        return String.format("%s@%s", this.getClass().getSimpleName(), this.hashCode());
    }

    @FunctionalInterface
    private interface Consumer {
        void accept(Field field) throws IllegalAccessException;
    }
}
