package cf;

public class RollbackError extends Error {
    public RollbackError(String str) {
        super(str);
    }
}
